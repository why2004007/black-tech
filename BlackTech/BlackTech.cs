﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace BPHelper
{
    class BlackTech
    {
        // Import Unmanaged Win32 Functions
        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll")]
        public static extern bool ReadProcessMemory(int hProcess,
          Int64 lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesRead);

        [DllImport("Kernel32.dll")]
        public static extern bool CloseHandle(int hObject);

        
        private const int PROCESS_WM_READ = 0x0010;
        // Process's handle
        private int handle;
        // 64bit address support
        private Int64 baseAddr;

        public BlackTech(Process process)
        {
            baseAddr = process.MainModule.BaseAddress.ToInt64();
            handle = (int)OpenProcess(PROCESS_WM_READ, false, process.Id);
        }

        public byte[] readBytes(Int64 address, int size = 4, bool relative = true)
        {
            var buffer = new byte[size];

            ReadProcessMemory(handle, relative ? baseAddr + address : address, buffer, size, ref size);

            return buffer;

        }

        public Int64 readInt64(Int64 address, bool relative = true)
        {
            int size = 8;
            byte[] buffer = new byte[size];

            ReadProcessMemory(handle, relative ? baseAddr + address : address, buffer, size, ref size);

            return BitConverter.ToInt64(buffer, 0);
        }

        public Int32 readInt32(Int64 address, bool relative = true)
        {
            int size = 4;
            byte[] buffer = new byte[size];

            ReadProcessMemory(handle, relative ? baseAddr + address : address, buffer, size, ref size);

            return BitConverter.ToInt32(buffer, 0);
        }

        public Int16 readInt16(Int64 address, bool relative = true)
        {
            int size = 2;
            byte[] buffer = new byte[size];

            ReadProcessMemory(handle, relative ? baseAddr + address : address, buffer, size, ref size);

            return BitConverter.ToInt16(buffer, 0);
        }

        public int readInt8(Int64 address, bool relative = true)
        {
            int size = 1;
            byte[] buffer = new byte[size];

            ReadProcessMemory(handle, relative ? baseAddr + address : address, buffer, size, ref size);

            return (int)buffer[0];
        }

        public String readString(Int64 address, Encoding encoding, int length = 9, bool relative = true)
        {
            byte[] buffer = new byte[length];

            ReadProcessMemory(handle, relative ? baseAddr + address : address, buffer, length, ref length);

            String str = encoding.GetString(buffer);
            if (str.IndexOf("\0") != -1)
            {
                str = str.Remove(str.IndexOf("\0"));
            }

            return str;

        }

        public void closeControl()
        {
            if (CloseHandle(handle))
            {
                Debug.WriteLine("Handle closed successfully.");
            }
            else
            {
                Debug.WriteLine("Failed to close handle.");
            }
        }

    }
}
