# Project Black Tech

Black Tech is a memory manipulating library with 64bit support. Its aim is to read, write and inject code into target process's memory.

# Current Version:
Ver 0.1 Alpha

# Current Feature:
Read Target Process Memory(int32, int64, string etc.)

# Incoming Features:
* Write to memory
* Search memory
* Inline ASM Code Injection

# License
Apache 2.0